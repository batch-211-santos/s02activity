
ssh-keygen
>>generates public or private rsa key pair

cat ~/.ssh/id_rsa.pub | clip
>>print ssh key, "clip" automatically copies it to clipboard

cd
>>change to parent directory (c/Users/<name>/)

cd ..
>>go up one level of the directory tree

cd ~/OneDrive/Desktop
>>change to Desktop directory - (Dekstop is moved to OneDrive)
>>this moves you to c/Users/<name>/OneDrive/Desktop

in ls (Windows)
>>light blue - hidden/protected system operating files
>>blue - folders
>>green - links/shortcuts
>>gray - files

ls -a
>>show hiddenfiles on current directory

git branch -a